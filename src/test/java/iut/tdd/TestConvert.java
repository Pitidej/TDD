package iut.tdd;

import junit.framework.Assert;

import org.junit.Test;

public class TestConvert {
	@Test
	public void test_num2text_zero () {
		Assert.assertEquals("zéro", Convert.num2text("0"));
	}
	
	@Test
	public void test_num2text_un () {
		Assert.assertEquals("un", Convert.num2text("1"));
	}
	
	@Test
	public void test_num2text_deux () {
		Assert.assertEquals("deux", Convert.num2text("2"));
	}
	
	@Test
	public void test_num2text_trois () {
		Assert.assertEquals("trois", Convert.num2text("3"));
	}
	
	@Test
	public void test_num2text_quatre () {
		Assert.assertEquals("quatre", Convert.num2text("4"));
	}
	
	@Test
	public void test_num2text_cinq () {
		Assert.assertEquals("cinq", Convert.num2text("5"));
	}
	
	@Test
	public void test_num2text_six () {
		Assert.assertEquals("six", Convert.num2text("6"));
	}
	
	@Test
	public void test_num2text_sept () {
		Assert.assertEquals("sept", Convert.num2text("7"));
	}
	
	@Test
	public void test_num2text_huit () {
		Assert.assertEquals("huit", Convert.num2text("8"));
	}
	
	@Test
	public void test_num2text_neuf () {
		Assert.assertEquals("neuf", Convert.num2text("9"));
	}
	
	@Test
	public void test_num2text_dix () {
		Assert.assertEquals("dix", Convert.num2text("10"));
	}
	
	@Test
	public void test_num2text_onze () {
		Assert.assertEquals("onze", Convert.num2text("11"));
	}
	
	@Test
	public void test_num2text_douze () {
		Assert.assertEquals("douze", Convert.num2text("12"));
	}
	
	@Test
	public void test_num2text_trenteetun () {
		Assert.assertEquals("trente et un", Convert.num2text("31"));
	}
	
	@Test
	public void test_num2text_troismilledeuxcentvingtquatre () {
		Assert.assertEquals("trois-mille-deux-cent-vingt-quatre", Convert.num2text("3224"));
	}
	
	@Test
	public void test_num2text_deuxcenttrois () {
		Assert.assertEquals("deux-cent-trois", Convert.num2text("203"));
	}
	
	@Test
	public void test_num2text_centtrois () {
		Assert.assertEquals("cent-trois", Convert.num2text(" 103"));
	}
	
	@Test
	public void test_num2text_treizemilletroiscentvingtsept () {
		Assert.assertEquals("treize-mille-trois-cent-vingt-sept", Convert.num2text("13327"));
	}
	
	@Test
	public void test_text2num_un () {
		Assert.assertEquals("1", Convert.text2num("un"));
	}
	
	@Test
	public void test_text2num_treizemilletroiscentvingtsept () {
		Assert.assertEquals("13327", Convert.text2num("treize-mille-trois-cent-vingt-sept"));
	}
	
	@Test
	public void test_soixante_dix() {
		Assert.assertEquals("soixante-dix", Convert.num2text("70"));
		Assert.assertEquals("soixante et onze", Convert.num2text("71"));
		Assert.assertEquals("soixante-douze", Convert.num2text("72"));
		Assert.assertEquals("soixante-treize", Convert.num2text("73"));
		Assert.assertEquals("soixante-quatorze", Convert.num2text("74"));
		Assert.assertEquals("soixante-quinze", Convert.num2text("75"));
		Assert.assertEquals("soixante-seize", Convert.num2text("76"));
		Assert.assertEquals("soixante-dix-sept", Convert.num2text("77"));
		Assert.assertEquals("soixante-dix-huit", Convert.num2text("78"));
		Assert.assertEquals("soixante-dix-neuf", Convert.num2text("79"));
	}
	
	@Test
	public void test_quatre_vingts() {
		Assert.assertEquals("quatre-vingts", Convert.num2text("80"));
		Assert.assertEquals("quatre-vingt-un", Convert.num2text("81"));
		Assert.assertEquals("quatre-vingt-deux", Convert.num2text("82"));
		Assert.assertEquals("quatre-vingt-trois", Convert.num2text("83"));
		Assert.assertEquals("quatre-vingt-quatre", Convert.num2text("84"));
		Assert.assertEquals("quatre-vingt-cinq", Convert.num2text("85"));
		Assert.assertEquals("quatre-vingt-six", Convert.num2text("86"));
		Assert.assertEquals("quatre-vingt-sept", Convert.num2text("87"));
		Assert.assertEquals("quatre-vingt-huit", Convert.num2text("88"));
		Assert.assertEquals("quatre-vingt-neuf", Convert.num2text("89"));
	}
	
}
