package iut.tdd;

public class Convert {

	private static final String[] nombres = { "zéro", "un", "deux", "trois",
			"quatre", "cinq", "six", "sept", "huit", "neuf", "dix", "onze",
			"douze", "treize", "quatorze", "quinze", "seize", "dix-sept",
			"dix-huit", "dix-neuf" };

	private static final String[] dizaines = { "", "dix", "vingt", "trente",
			"quarante", "cinquante", "soixante", "soixante-dix",
			"quatre-vingt", "quatre-vingt-dix" };

	private static final String[] centaines = { "", "cent", "deux-cent",
			"trois-cent", "quatre-cent", "cinq-cent", "six-cent", "sept-cent",
			"huit-cent", "neuf-cent" };

	private static final String[] millier = { "", "mille", "deux-mille",
			"trois-mille", "quatre-mille", "cinq-mille", "six-mille",
			"sept-mille", "huit-mille", "neuf-mille" };

	public static String num2text(String input) {
		String retour = "";
		if (input.length() == 2 || input.length() == 1) {
			retour += convertionDeuxChiffres(input);
		}
		if (input.length() == 3) {
			retour += convertionTroisChiffres(input);
		}
		if (input.length() == 4) {
			retour += convertionQuatreChiffres(input);
		}
		if (input.length() == 5) {
			retour += convertionCinqChiffres(input);
		}
		return retour;
	}

	public static String convertionDeuxChiffres(String input) {
		int recup = Integer.parseInt(input);
		String renvoi = "";
		if (recup >= 0 && recup <= 19) {
			return nombres[recup];
		} else if (input.length() == 2) {
			if (Character.getNumericValue(input.charAt(1)) == 1 && Character.getNumericValue(input.charAt(0)) != 7 && Character.getNumericValue(input.charAt(0)) != 8) {
				renvoi += dizaines[Character.getNumericValue(input.charAt(0))]
						+ " et un";
			} else if (Character.getNumericValue(input.charAt(1)) == 0 && Character.getNumericValue(input.charAt(0)) != 8) {
				renvoi += dizaines[Character.getNumericValue(input.charAt(0))];
			} else if ((Character.getNumericValue(input.charAt(0)) == 7) && (Character.getNumericValue(input.charAt(1)) == 1)){
				return "soixante et onze";
			} else if ((Character.getNumericValue(input.charAt(0)) == 8) && (Character.getNumericValue(input.charAt(1)) == 0)){
				return "quatre-vingts";
			}else if (Character.getNumericValue(input.charAt(0)) == 7){
				return ""+dizaines[(Character.getNumericValue(input.charAt(0)))-1]+"-"+nombres[(Character.getNumericValue(input.charAt(1)))+10];
			} else if (Character.getNumericValue(input.charAt(0)) == 9){
				return ""+dizaines[(Character.getNumericValue(input.charAt(0)))-1]+"-"+nombres[(Character.getNumericValue(input.charAt(1)))+10];
			} else {
				recup = Character.getNumericValue(input.charAt(0));
				renvoi += dizaines[recup] + "-";
				recup = Character.getNumericValue(input.charAt(1));
				renvoi += nombres[recup];
			}
		}
		return renvoi;
	}

	public static String convertionTroisChiffres(String input) {
		int recup;
		String renvoi = "";
		recup = Character.getNumericValue(input.charAt(0));
		renvoi += centaines[recup] + "-";
		renvoi += convertionDeuxChiffres(input.substring(1, 3));
		return renvoi;
	}

	public static String convertionQuatreChiffres(String input) {
		int recup;
		String renvoi = "";
		recup = Character.getNumericValue(input.charAt(0));
		renvoi += millier[recup] + "-";
		renvoi += convertionTroisChiffres(input.substring(1, 4));
		return renvoi;
	}

	public static String convertionCinqChiffres(String input) {
		String renvoi = "";
		renvoi += convertionDeuxChiffres(input.substring(0, 2)) + "-mille-";
		renvoi += convertionTroisChiffres(input.substring(2, 5));
		return renvoi;
	}

	public static String text2num(String input) {
		boolean zorro = false;
		int cpt = 0;
		while (!zorro) {
			if (num2text(Integer.toString(cpt)).equals(input)) {
				return Integer.toString(cpt);
			}
			cpt++;
		}
		return null;

	}
}